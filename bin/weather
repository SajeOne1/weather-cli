#!/usr/bin/env python

# weather-cli
# Copyright (C) 2021 Shane Brown
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import sys
from datetime import datetime

from weather.Current import Current
from weather.Hourly import Hourly
from weather.Monthly import Monthly

CURRENT_MIN_PRECISION = 3
HOURLY_MIN_PRECISION = 3
MONTHLY_MIN_PRECISION = 2

# Used to check precision of given datetime
precision = 7

def parse_datetime(dt):
    global precision

    # Maximum precision (rare)
    try:
        timeframe = datetime.strptime(args.date, '%Y-%m-%dT%H:%M:%S%z')
        precision = 7
        return timeframe
    except ValueError:
        pass

    # Date and time
    try:
        timeframe = datetime.strptime(args.date, '%Y-%m-%dT%H:%M:%S')
        precision = 6
        return timeframe
    except ValueError:
        pass

    # Exact date
    try:
        timeframe = datetime.strptime(args.date, '%Y-%m-%d')
        precision = 3
        return timeframe
    except ValueError:
        pass

    # Minimum precision (month + year)
    try:
        timeframe = datetime.strptime(args.date, '%Y-%m')
        precision = 3
        return timeframe
    except ValueError:
        sys.stderr.write("Error, malformed date, please refer to ISO-8601\n")
        return None

parser = argparse.ArgumentParser(description="Weather at your fingers")
parser.add_argument("timeframe", nargs='?', help="current (default), hourly, monthly")
parser.add_argument("--region-code", help="weather region code to determine weather location")
parser.add_argument("--date", help="specify custom date to get weather for instead of current (ISO-8601)")
parser.add_argument("--verbose", action="store_true", help="output more info")

args = parser.parse_args()

region_code = "caon0493" # Oakville
timeframe = datetime.now()

# Parse custom date
if args.date:
    timeframe = parse_datetime(args.date)

# Set default timeframe
if not args.timeframe:
    args.timeframe = "current"

check_timeframe = args.timeframe.lower()
if check_timeframe == "current":
    if precision < CURRENT_MIN_PRECISION:
        sys.stderr.write("--date param must be more specific to show 'current' weather\n")
        sys.exit(1)

    current_provider = Current(region_code, timeframe)
    resp = current_provider.process_weather()
    if not resp:
        sys.stderr.write("ERROR: \"" + current_provider.error + "\"\n")
        sys.exit(1)

    sys.stdout.write(resp)
elif check_timeframe == "hourly":
    if precision < HOURLY_MIN_PRECISION:
        sys.stderr.write("--date param must be more specific to show 'hourly' weather\n")
        sys.exit(1)

    hourly_provider = Hourly(region_code, timeframe, args.verbose)
    resp = hourly_provider.process_weather()
    if not resp:
        sys.stderr.write("ERROR: \"" + hourly_provider.error + "\"\n")
        sys.exit(1)

    sys.stdout.write(resp)
elif check_timeframe == "monthly":
    if precision < MONTHLY_MIN_PRECISION:
        sys.stderr.write("--date param must be more specific to show 'monthly' weather\n")
        sys.exit(1)

    monthy_provider = Monthly(region_code, timeframe)
    resp = monthy_provider.process_weather()
    if not resp:
        sys.stderr.write("ERROR: \"" + monthy_provider.error + "\"\n")
        sys.exit(1)

    sys.stdout.write(resp)
else:
    sys.stderr.write("ERROR: Unrecognized timeframe\n\n")
    parser.print_help()
    sys.exit(1)
