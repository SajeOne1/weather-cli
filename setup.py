#!/usr/bin/env python

# weather-cli
# Copyright (C) 2021 Shane Brown
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

setup(
    name="weather-cli", # Replace with your own username
    version="1.0",
    author="Shane Brown",
    author_email="contact@shanebrown.ca",
    description="Weather at your fingertips",
    url="https://gitlab.com/SajeOne1/weather-cli",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    scripts = ['bin/weather'],
    packages=['weather'],
    python_requires=">=3.6",
    install_requires=['requests'],
)
