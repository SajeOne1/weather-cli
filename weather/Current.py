#!/usr/bin/env python

# weather-cli
# Copyright (C) 2021 Shane Brown
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import json
import base64
from datetime import datetime
from base64 import b64decode as bdecode

from weather.WeatherProvider import WeatherProvider

BASE = "aHR0cHM6Ly93ZWF0aGVyYXBpLnBlbG1vcmV4LmNvbQ=="

class Current(WeatherProvider):
    region_code = None
    result = None
    def __init__(self, region_code, timeframe=None):
        self.region_code = region_code

        if timeframe:
            self.timeframe = timeframe
        else:
            self.timeframe = datetime.now()

        self.url = bdecode(BASE).decode('utf-8') + "/api/v1/observation/placecode/" + self.region_code + "?locale=en-ca&unit=metric"

    def process_weather(self):
        resp = requests.get(self.url)
        self.status_code = resp.status_code

        if resp.status_code != 200:
            self.error = "Failed to download weather data"
            return None

        if resp.content.decode('utf-8') == "\"NO DATA\"":
            self.error = "No weather data present for given date"
            return None

        current_weather = self.__parse_json(resp.content)
        if not current_weather:
            return None

        self.result = "-=Current Weather for " + self.timeframe.strftime('%B %Y') + "=-\n"
        self.result = self.result + current_weather['condition'] + " : " + str(current_weather['temperature']) + " degrees : Feels like " + str(current_weather['feels_like'])
        self.result = self.result + "\n\nUpdated: " + current_weather['observation'].strftime('%I:%M %p') + "\n"
        return self.result

    def get_result(self):
        return self.result

    def __parse_json(self, jsonTxt):
        # Error guards
        try:
            obj = json.loads(jsonTxt)
        except json.JSONDecodeError:
            self.error = "Failed to parse json"
            return None

        current_obj = dict()

        current_obj['observation'] = datetime.strptime(obj['observation']['time']['local'], '%Y-%m-%dT%H:%M')
        current_obj['condition'] = obj['observation']['weatherCode']['text']
        current_obj['temperature'] = obj['observation']['temperature']
        current_obj['feels_like'] = obj['observation']['feelsLike']

        return current_obj
