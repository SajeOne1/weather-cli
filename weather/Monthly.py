#!/usr/bin/env python

# weather-cli
# Copyright (C) 2021 Shane Brown
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import json
from datetime import datetime
from base64 import b64decode as bdecode

from weather.WeatherProvider import WeatherProvider

BASE = "aHR0cHM6Ly93d3cudGhld2VhdGhlcm5ldHdvcmsuY29t"

class Monthly(WeatherProvider):
    region_code = None
    result = None
    def __init__(self, region_code, timeframe=None):
        self.region_code = region_code

        if timeframe:
            self.timeframe = timeframe
        else:
            self.timeframe = datetime.now()

        self.url = bdecode(BASE).decode('utf-8') + "/api/historical/" + region_code + "/" + str(self.timeframe.month).zfill(2) + "/" + str(self.timeframe.year) + "/C/metric"

    def process_weather(self):
        resp = requests.get(self.url)
        self.status_code = resp.status_code

        if resp.status_code != 200:
            self.error = "Failed to download weather data"
            return None

        if resp.content.decode('utf-8') == "\"NO DATA\"":
            self.error = "No weather data present for given date"
            return None

        temp_list = self.__parse_json(resp.content)

        self.result = "-=Daily Weather for " + self.timeframe.strftime('%B %Y') + "=-\n"
        for item in temp_list:
            self.result = self.result + item['day'] + " : " + item['temp_max'] + " (" + item['temp_min'] + ") " + item['additional'] + "\n"

        return self.result


    def get_result(self):
        return self.result

    def __parse_json(self, jsonTxt):
        temp_list = list()
        nums = list()
        obj = None

        # Error guards
        try:
            obj = json.loads(jsonTxt)
        except json.JSONDecodeError:
            self.error = "Failed to parse json"
            return None

        if obj['status'] != 200:
            self.error = "Server returned error"
            return None

        # Filter non-nums
        for item in obj.keys():
            try:
                int(item)
                nums.append(item)
            except ValueError:
                pass

        # Parse daily temp values
        for item in nums:
            cur_obj = obj[item]
            day = dict()

            # For some reason provider populates feb 29th on non-leap years and excludes this key, lol
            if 'day_short' in cur_obj:
                day['day'] = cur_obj['day_short'] + " " + item
            else:
                day['day'] = "N/A " + item

            day['additional'] = ""
            if cur_obj['rdata']['temperatureMax']: # Forecast or recorded data
                day['temp_max'] = cur_obj['rdata']['temperatureMax']
                day['temp_min'] = cur_obj['rdata']['temperatureMin']
            else: # Expected, no forecast
                day['temp_max'] = "~" + cur_obj['rdata']['ex_tempMax']
                day['temp_min'] = "~" + cur_obj['rdata']['ex_tempMin']

            if 'icon_text_day' in cur_obj['rdata']:
                day['additional'] = cur_obj['rdata']['icon_text_day']

            if 'popDay_percent' in cur_obj['rdata']:
                day['additional'] = day['additional'] + " " + cur_obj['rdata']['popDay_percent'] + "%☔"
            elif 'pop_percent' in cur_obj['rdata']:
                day['additional'] = day['additional'] + " " + cur_obj['rdata']['pop_percent'] + "%☔"

            temp_list.append(day)

        return temp_list
