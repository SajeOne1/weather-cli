#!/usr/bin/env python

# weather-cli
# Copyright (C) 2021 Shane Brown
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import json
from datetime import datetime, timezone
from base64 import b64decode as bdecode

from weather.WeatherProvider import WeatherProvider

BASE = "aHR0cHM6Ly93d3cudGhld2VhdGhlcm5ldHdvcmsuY29t"

class Hourly(WeatherProvider):
    region_code = None
    result = None
    def __init__(self, region_code, timeframe=None, verbose=False):
        self.region_code = region_code
        self.verbose = verbose

        if timeframe:
            self.timeframe = timeframe
        else:
            self.timeframe = datetime.now()

        offset = self.__generate_offset()
        self.url = bdecode(BASE).decode('utf-8') + "/api/data/" + region_code + "/hourly/cm/ci?ts=" + offset

    def process_weather(self):
        resp = requests.get(self.url)
        self.status_code = resp.status_code

        if resp.status_code != 200:
            self.error = "Failed to download weather data"
            return None

        if resp.content.decode('utf-8') == "\"NO DATA\"":
            self.error = "No weather data present for given date"
            return None

        hourly_weather = self.__parse_json(resp.content)
        if not hourly_weather:
            return None

        self.result = "-=Hourly Weather for " + self.timeframe.strftime('%B %Y') + "=-\n"
        for item in hourly_weather:
            self.result = self.result + item['day'] + " " + item['hour'] + ": " + item['condition'] + " : " + str(item['temperature']) + " degrees : Feels like " + str(item['feels_like']) + " : " + str(item['rain']['value']) + " " + item['rain']['unit'] + " " + item['precip'] + "%☔\n"

        return self.result

    def get_result(self):
        return self.result

    def __parse_json(self, jsonTxt):
        # Error guards
        try:
            obj = json.loads(jsonTxt)
        except json.JSONDecodeError:
            self.error = "Failed to parse json"
            return None

        temp_list = list()
        for item in obj['hourly']['periods']:
            temp_date = datetime.strptime(item['cdate'], "%A, %B %d")
            if not self.verbose and (self.timeframe.day != temp_date.day or self.timeframe.month != temp_date.month):
                continue

            temp_obj = dict()
            temp_obj['day'] = item['dayname_alt']
            temp_obj['hour'] = item['hour']
            temp_obj['rain'] = {'value': item['rain_value'], 'unit': item['ru']}
            temp_obj['condition'] = item['it']
            temp_obj['temperature'] = item['t']
            temp_obj['feels_like'] = item['f']
            temp_obj['precip'] = item['pp']
            temp_list.append(temp_obj)

        return temp_list

    # Generates time offset needed by provider
    def __generate_offset(self):
        utc_time = datetime.now(timezone.utc)
        theHour = utc_time.hour
        theCurrentMinute = utc_time.minute
        offset = 52

        if theCurrentMinute <= 12:
            theHour = theHour - 1
            if theHour < 10:
                theHour = str(theHour).zfill(2)
        elif theCurrentMinute > 12 and theCurrentMinute <= 32:
            offset = 12
        elif theCurrentMinute > 32 and theCurrentMinute <= 52:
            offset = 32

        return str(theHour) + str(offset)
